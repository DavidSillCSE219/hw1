package tdlm.workspace.foolproof;

import djf.modules.AppGUIModule;
import djf.ui.foolproof.FoolproofDesign;
import javafx.scene.control.TextField;
import tdlm.ToDoListMakerApp;
import static tdlm.ToDoPropertyType.TDLM_OWNER_TEXT_FIELD;
import static tdlm.ToDoPropertyType.TDLM_REMOVE_ITEM_BUTTON;
import static tdlm.ToDoPropertyType.TDLM_EDIT_ITEM_BUTTON;
import static tdlm.ToDoPropertyType.TDLM_MOVE_ITEM_UP_BUTTON;
import static tdlm.ToDoPropertyType.TDLM_MOVE_ITEM_DOWN_BUTTON;
import tdlm.data.ToDoData;

/**
 *
 * @author McKillaGorilla
 */
public class ToDoSelectionFoolproofDesign implements FoolproofDesign {
    ToDoListMakerApp app;
    
    public ToDoSelectionFoolproofDesign(ToDoListMakerApp initApp) {
        app = initApp;
    }

    @Override
    public void updateControls() {
        AppGUIModule gui = app.getGUIModule();
       
        // CHECK AND SEE IF A TABLE ITEM IS SELECTED
        ToDoData data = (ToDoData)app.getDataComponent();
        boolean itemIsSelected = data.isItemSelected();
        boolean itemsAreSelected = data.areItemsSelected();
        boolean itemAtTop = data.getItemIndex(data.getSelectedItem()) == 0;
        boolean itemAtBottom = data.getItemIndex(data.getSelectedItem()) == data.getNumItems() - 1;
        gui.getGUINode(TDLM_REMOVE_ITEM_BUTTON).setDisable(!(itemIsSelected || itemsAreSelected));
        gui.getGUINode(TDLM_EDIT_ITEM_BUTTON).setDisable(!itemIsSelected);
        gui.getGUINode(TDLM_MOVE_ITEM_UP_BUTTON).setDisable((!itemIsSelected) || itemAtTop);
        gui.getGUINode(TDLM_MOVE_ITEM_DOWN_BUTTON).setDisable((!itemIsSelected) || itemAtBottom);
        ((TextField)gui.getGUINode(TDLM_OWNER_TEXT_FIELD)).setEditable(true);
    }
}