package tdlm.transactions;

import jtps.jTPS_Transaction;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class MoveItemTransaction implements jTPS_Transaction {
    ToDoData data;
    ToDoItemPrototype itemToMove;
    int move;
    int initIndex;
    
    
    public MoveItemTransaction(ToDoData initData, ToDoItemPrototype item, int move) {
        data = initData;
        itemToMove = item;
        this.move = move;
        initIndex = data.getItemIndex(itemToMove);
        if(move > 1){
            move = 1;
        } else if(move < -1){
            move = -1;
        }
        
        
    }

    @Override
    public void doTransaction() {
        data.moveItem(initIndex, initIndex + move);
        data.clearSelected();
        data.selectItem(itemToMove);
        
    }

    @Override
    public void undoTransaction() {
        data.moveItem(initIndex + move, initIndex);
        data.clearSelected();
    }
}
