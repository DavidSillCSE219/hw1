package tdlm.transactions;

import jtps.jTPS_Transaction;
import tdlm.data.ToDoData;
import tdlm.data.ToDoItemPrototype;

/**
 *
 * @author McKillaGorilla
 */
public class EditItem_Transaction implements jTPS_Transaction {
    ToDoData data;
    ToDoItemPrototype itemToAdd;
    ToDoItemPrototype itemToRemove;
    
    public EditItem_Transaction(ToDoData initData, ToDoItemPrototype newData, ToDoItemPrototype oldData) {
        data = initData;
        itemToAdd = newData;
        itemToRemove = oldData;
    }

    @Override
    public void doTransaction() {
        data.addItem(itemToAdd);
        data.removeItem(itemToRemove);
    }

    @Override
    public void undoTransaction() {
        data.removeItem(itemToAdd);
        data.addItem(itemToRemove);
    }
}
